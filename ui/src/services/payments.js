import axios from "axios";

const userServiceBaseUrl = "http://localhost:8080";

// gets state array of objects, with uuid, appuuid, payMethod, Payamount

export const getPayments = async () => {
  const { data } = await axios.get(`${userServiceBaseUrl}/payments`);
  return data;
};

//creates new payment and adds to our state/database, passing appUuid and payAmount

export const createPayment = async ({ applicationUuid, requestedAmount }) => {
  const { data } = await axios.post(`${userServiceBaseUrl}/payments`, {
    applicationUuid,
    paymentAmount: requestedAmount,
  });
  return data;
};
