import axios from "axios";

const userServiceBaseUrl = "http://localhost:8080";

// gets all application data which includes uuid, useruuid and requestedAmount, saved in an array of objects 

export const getApplications = async () => {
  const { data } = await axios.get(`${userServiceBaseUrl}/applications`);
  return data;
};
