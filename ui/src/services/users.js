import axios from "axios";

const userServiceBaseUrl = "http://localhost:8080";

//gets users from db that includes, uuid, name, email

export const getUsers = async () => {
  const { data } = await axios.get(`${userServiceBaseUrl}/users`);
  return data;
};
