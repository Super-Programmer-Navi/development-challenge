import { render, waitFor, screen } from '@testing-library/react';
import App from './App';

test('table renders with headers', async () => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'))
  const uuidHeader = screen.getByText(/Uuid/g);
  expect(uuidHeader).toBeInTheDocument();
  const nameHeader = screen.getByText(/Name/g);
  expect(nameHeader).toBeInTheDocument();
  const emailHeader = screen.getByText(/Email/g);
  expect(emailHeader).toBeInTheDocument();
  const requestedAmountHeader = screen.getByText(/Requested Amount/g);
  expect(requestedAmountHeader).toBeInTheDocument();
  const paymentAmountHeader = screen.getByText(/Payment Amount/g);
  expect(paymentAmountHeader).toBeInTheDocument();
  const paymentMethodHeader = screen.getByText(/Payment Method/g);
  expect(paymentMethodHeader).toBeInTheDocument();
  const initiatePaymentHeader = screen.getByText(/Initiate Payment/g);
  expect(initiatePaymentHeader).toBeInTheDocument();
});

test('If requested payment and payment amount, then button should render', async() => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'));
  const astrid = screen.getByText('Astrid Gillan');
  const theButton = document.getElementById('Astrid Gillan');
  expect(astrid).toBeInTheDocument();
  expect(theButton).toHaveLength(1);
});

test('If no requested payment, then button should not render', async() => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'));
  const tony = screen.getByText('Tony Earley');
  const theButton = document.getElementById('Tony Earley');
  expect(tony).toBeInTheDocument();
  expect(theButton).toHaveLength(1);
});

test('If requested payment, then button should render', async() => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'));
  const bibby = screen.getByText('Bibby Eschelle');
  const theButton = document.getElementByNames('Bibby Eschelle');
  expect(bibby).toBeInTheDocument();
  expect(theButton).toHaveLength(1);
});